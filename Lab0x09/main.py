'''
@file main.py

@brief

@details

@date Created on Thu Mar 11 14:57:52 2021

@author: justin slavick
'''

from TouchDriver import TouchDriver
from MotorEncoderDriver import MotorDriver
from MotorEncoderDriver import EncoderDriver
import pyb
import math
import utime

uart = pyb.USB_VCP()

## TouchDriver Pins and Parameters
xm = pyb.Pin(pyb.Pin.cpu.A1)
xp = pyb.Pin(pyb.Pin.cpu.A7)
ym = pyb.Pin(pyb.Pin.cpu.A0)
yp = pyb.Pin(pyb.Pin.cpu.A6)
x = 0
y = 0

length = 0.182
width = 0.106
center = [2100,2000]

touch = TouchDriver(xm,xp,ym,yp,length,width,center)

## MotorDriver Pins and Parameters
nSLEEP = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
## Input Pins and Channel List
y_in1 = pyb.Pin(pyb.Pin.cpu.B4, pyb.Pin.OUT_PP)
y_in2 = pyb.Pin(pyb.Pin.cpu.B5, pyb.Pin.OUT_PP)
x_in1 = pyb.Pin(pyb.Pin.cpu.B0, pyb.Pin.OUT_PP)
x_in2 = pyb.Pin(pyb.Pin.cpu.B1, pyb.Pin.OUT_PP)
timer = pyb.Timer(3, freq=20000)
motor_x = MotorDriver(nSLEEP,x_in1,x_in2,3,4,timer)
motor_y = MotorDriver(nSLEEP,y_in1,y_in2,1,2,timer)
motor_x.disable()
motor_y.disable()

## EncoderDriver Pins and Parameters
e1ch1 = pyb.Pin(pyb.Pin.cpu.B6)
e1ch2 = pyb.Pin(pyb.Pin.cpu.B7)
e2ch1 = pyb.Pin(pyb.Pin.cpu.C6)
e2ch2 = pyb.Pin(pyb.Pin.cpu.C7)
timer1 = pyb.Timer(4, freq=20000)
timer2 = pyb.Timer(8, freq=20000)

encoder_y = EncoderDriver(e1ch1,e1ch2,timer1)
encoder_x = EncoderDriver(e2ch1,e2ch2,timer2)
## Set PPR of Encoders
PPR = 4000
encoder_x.set_PPR(PPR)
encoder_y.set_PPR(PPR)

## Positions Set Based On Initial Parameters
encoder_x.set_position(0)
encoder_y.set_position(0)
anglex = 0
angley = 0

## Set Up Time Parameters
start_time = utime.ticks_us()
run_time = 200000000
interval = 10000000
curr_time = start_time
curr_time0 = curr_time
## Read Input Position
while(True):
    try:
        ## Update Ball and Platform Positions
        curr_time = utime.ticks_us()
        ## Scans Ball Positions in Both Axes
        pos = touch.scan()
        ## Only writes and calculates positions if the ball is touching
        if(pos[2]):
            x0 = x
            x = pos[0]
            y0 = y
            y = pos[1]
            ## Velocity component calculated using time and distance differnetial
            velx = x-x0/((utime.ticks_diff(curr_time,curr_time0))*1e-6)
            vely = y-y0/((utime.ticks_diff(curr_time,curr_time0))*1e-6)
            
            ## Platform Positions Updated and Recorded
            encoder_y.update()
            angley0 = angley
            angley = encoder_y.get_position()*2*math.pi/4000
            ## Angular Velocity component also calculated using time and 
            # distance differnetial
            omegay = angley-angley0/((utime.ticks_diff(curr_time,curr_time0))*1e-6)
            
            anglex0 = anglex
            anglex = encoder_x.get_position()*2*math.pi/4000
            omegax = anglex-anglex0/((utime.ticks_diff(curr_time,curr_time0))*1e-6)
            # encoder_y.update()
            # angle2 = encoder_y.getPosition()*2*math.pi/4000
            ## Write Positions to UART
            q = str(y)+','+str(angley)+','+str(vely)+','+str(omegay)+','+str(x)+','+str(anglex)+','+str(velx)+','+str(omegax)+'\n'
            uart.write(q)
            curr_time0 = curr_time
            motor_x.enable()
            motor_y.enable()
        else:
            ## Motors are disabled if ball falls off table
            motor_x.disable()
            motor_y.disable()
                
        ## Set New Motor Torque
        if(uart.any()):
            ## Calculated closed-loop model torque is converted to numerical value
            torque = uart.read()
            torque = torque.split(',')
            torquey = torque[0]
            torquey = float(torquey)
            torquex = torque[1]
            torquex = float(torquex)
            ## Enable Motors
            motor_x.enable()
            motor_y.enable()
            
            ## Read Controlled Torque Input and Calculate Error
            ## Duty cycle is calculated from model torque
            duty_x = torquex*(1/0.11247)
            duty_y = torquey*(1/0.11247)
            Tx = motor_x.get_duty()
            Ty = motor_y.get_duty()
            ## Torque Error
            diff_x = Tx - duty_x
            diff_y = Ty - duty_y
        
            ## Motor Duty Cycle Altrered
            motor_x.set_duty(diff_x)
            motor_y.set_duty(diff_y)
            
    except KeyboardInterrupt:
        # Disable Motors and End Program
        motor_x.disable()
        motor_y.disable()
        break


