'''
@file TermProject.py

@brief

@details Utilizes the code from Lab0x06 to set-up the controller gains and linearize

@date Created on Tue Mar 16 01:30:37 2021

@author: justin slavick
'''

from openloop import Linearization
from openloop import Controller
import numpy as np
from sympy import *
import math
import time
import serial

## Set Up Serial Communication
ser = serial.Serial(port='COM5',baudrate=115273,timeout=1)

# ser.write(str(0).encode('ascii'))
# myval = ''
# while (myval == ''):
#     myval = ser.readline().decode('ascii')
#     print(myval)
# my_arr = myval.split(',')    
## Initial Variable Parameters
x_init = 0
xdot_init = 0
theta_init = 0
thetadot_init = 0
## Initial State Space Array
q_init = np.array([[x_init],[theta_init],[xdot_init],[thetadot_init]])
## Initial Motor Torque
Tx_init = 0

## Simulation Runtime (s)
runtime = 20

## Model Constants
rm = 0.06
lr = 0.05
rb = 0.0105
rg = 0.042
lp = 0.110
rp = 0.0325
rc = 0.05
mb = 0.03
mp = 0.4
Ip = 0.00188
b = 0.01
constants = [rm,lr,rb,rg,lp,rp,rc,mb,mp,Ip,b]

## Matrix Formation and Jacobian Linearization
lin = Linearization(constants,q_init,Tx_init)
dq = lin.stateSpace()
A = lin.jacobian(True)
B = lin.jacobian(False)

## Solve for Natural Frequency and Damping Coefficient in terms of Desired Max Peak and Settling Time
## Max Peak in m
# Mp = 0.001
## Settling Time in s
# ts = 0.5

##Solve Separately
z = 1
wn = 1
# wn = symbols('wn')
# z = symbols('z')
# peak_char = solve((exp(-(math.pi*z/sqrt(1-z**2))))-Mp,z)

## Controller Creation
control = Controller(A,B,wn,z)

## Find Controller Gains
## Lab 0x06 Gains
# K = np.array([[-0.3,-0.2,-0.05,-0.02]])

## Gains found from Poles
gains = control.findKs()
## Pop Controller Gains from Set
gains = gains[1].pop()
K1 = gains[0]
K2 = gains[1]
K3 = gains[2]
K4 = gains[3]
K = np.array([[K1,K2,K3,K4]])

## Sets up initial time parameters for loop
start_time = time.time()
time_dist = [start_time-start_time]
curr_time = start_time
runs = 0

Tx = Tx_init
while(curr_time-start_time <= runtime):
    ## Run the simulation at that time step
    ## Exception is so that serial and motors can turn off when code ends
    try:
        ## Reads q matrix. Loops until there is a ball touching the touchpad
        q = ''
        while(q == ''):
            q = ser.readline().decode('ascii')
            ## String is separated and broken down into array
            q = q.split('\n')
            q = q[0].split(',')
            # print(q)
        ## Exception is so that non numerical values are skipped in the q matrix
        try:
            ## Array is sent as two combined q vector strings. The x and y
            # components are split based on location
            qx = np.array([[float(q[0])],[float(q[1])],[float(q[2])],[float(q[3])]])
            qy = np.array([[float(q[4])],[float(q[5])],[float(q[6])],[float(q[7])]])
            # print(qx)
            ## The torque matrix is constructed using the controller gains
            # for x and y
            Tx = -K@qx
            Tx = Tx[0][0]
            Ty = -K@qy
            Ty = Ty[0][0]
            ## A combined csv torque string is sent to the nucleo
            T = str(Tx) + ',' + str(Ty)
            ser.write(str(T).encode('ascii'))
            print(ser.readline().decode('ascii'))
        except ValueError:
            print('Skip')
            pass
    except KeyboardInterrupt:
        ser.write('\x03'.encode('ascii'))
        ser.close()
        break

