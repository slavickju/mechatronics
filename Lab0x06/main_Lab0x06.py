# -*- coding: utf-8 -*-
'''
@file main_Lab0x06.py

@brief

@details

@date Created on Tue Mar 16 01:30:37 2021

@author: justin slavick
'''

from openloop import Linearization
from openloop import Simulation
import numpy as np
from sympy import *
import math
import time
import matplotlib.pyplot as plt

## Initial Variable Parameters
x_init = 0.05
xdot_init = 0
theta_init = 0
thetadot_init = 0
## Initial State Space Array
q_init = np.array([[x_init],[theta_init],[xdot_init],[thetadot_init]])
## Initial Motor Torque
Tx_init = 0

## Simulation Runtime (s)
runtime = 20

## Model Constants
rm = 0.06
lr = 0.05
rb = 0.0105
rg = 0.042
lp = 0.110
rp = 0.0325
rc = 0.05
mb = 0.03
mp = 0.4
Ip = 0.00188
b = 0.01
constants = [rm,lr,rb,rg,lp,rp,rc,mb,mp,Ip,b]

## Matrix Formation and Jacobian Linearization
lin = Linearization(constants,q_init,Tx_init)
dq = lin.stateSpace()
A = lin.jacobian(True)
B = lin.jacobian(False)

## Sets up initial vector parameters for plot
xdot = [xdot_init]
thetadot = [thetadot_init]
x = [x_init]
theta = [theta_init]
stepmat = [0]

## Sets up initial time parameters for loop
start_time = time.time()
time_dist = [start_time-start_time]
curr_time = start_time
runs = 0

while(curr_time-start_time <= runtime):
    ## Run the simulation at that time step
    curr_time1 = curr_time
    curr_time = time.time()
    step = curr_time-curr_time1
    q = sim.run(step)
    Tx = -K@q
    # if (runs >= 100):
        # sim.setTx(0)
    sim.setTx(Tx)
    runs = runs+1
    
    ## Expand the lists holding time and variables
    x.append(q[0][0])
    theta.append(q[1][0])
    xdot.append(q[2][0])
    thetadot.append(q[3][0])
    stepmat.append(step)
    time_dist.append(curr_time-start_time)

## Creates and formats the plots for the four variables
fig = plt.figure()
plt.xlabel('Time (s)')
plt.ylabel('Ball Position (m)')
plt.title('Closed Loop Response of Ball Position')
plt.plot(time_dist, x)

fig = plt.figure()
plt.xlabel('Time (s)')
plt.ylabel('Ball Velocity (m/s)')
plt.title('Closed Loop Response of Ball Velocity')
plt.plot(time_dist, xdot)
    
fig = plt.figure()
plt.xlabel('Time (s)')
plt.ylabel('Table Tilt Angle (rad)')
plt.title('Closed Loop Response of Table Tilt Angle')
plt.plot(time_dist, theta)

fig = plt.figure()
plt.xlabel('Time (s)')
plt.ylabel('Table Tilt Angle (rad/s)')
plt.title('Closed Loop Response of Table Angular Velocity')
plt.plot(time_dist, thetadot)
