# -*- coding: utf-8 -*-
'''
@file simulation.py

@brief

@details

@date Created on Tue Mar 16 01:30:11 2021

@author: justin slavick
'''
from sympy import symbols

class Simulation():
    
    def __init__(self,A,B,q_init,Tx):
        self.A = A
        self.B = B
        
        self.q = q_init
        self.Tx = Tx
        
        self.step = 1e-6
        
        
    def run(self):
        x = symbols('x')
        theta = symbols('theta')
        xdot = symbols('xdot')
        thetadot = symbols('thetadot')
        Bu = (self.B.subs(self.Tx,self.Tx))@(self.Tx)
        Ax = (self.A.subs([(xdot,self.q[0]),(thetadot,self.q[1]),(x,self.q[2]),(theta,self.q[3])]))@(self.q)
        qdot = Ax + Bu
        q = qdot*self.step
        return q
    
    def getTx(self):
        return self.Tx
    
    def setTx(self, Tx):
        self.Tx = Tx
        
    def getA(self):
        return self.A
    
    def setA(self, A):
        self.A = A
        
    def getB(self):
        return self.B
        
    def setB(self,B):
        self.B = B
        
    


