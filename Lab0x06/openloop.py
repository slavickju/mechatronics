# -*- coding: utf-8 -*-
'''
@file openloop.py

@brief This file contains classes that linearize the system dynamic model and that simulate and control the response

@details The Linearization class in this file is responsible for turning the second-order balancing system to a first-order
linear system using Jacobian Linearization. The matrixConstruction method constructs the kinematic matrix and the stateSpace
method reorders the variables to be linearized. The jacobian method takes partial derivatives of each of the kinematic matrix
parameters with respect to each variable to linearize the system. These linearized matricies are then passed to the simulation
class to model how each variable changes over time in response to various input parameters. The getPoles and findK methods also
calculate the closed-loop controller gain values to maximize the performance of the system.

@date Created on Sun Feb 21 03:09:05 2021

@author: justin slavick
'''

import numpy as np
from sympy import *
import math
from scipy import io, integrate, linalg, signal
from scipy.sparse.linalg import eigs

class Linearization():
    
    def __init__(self,constants, q_init, Tx):
        '''
        @brief Creates variables representing the system characteristics and initial conditions
        
        @details This class recieves an input of a list of constants, that
        represents all of the hardware values needed to model the system.
        The initial conditions are also passed into a list of variables that
        will serve to numericallize the jacobian matricies.

        '''
        ## Constants that represent system parameters
        self.rm = constants[0]
        self.lr = constants[1]
        self.rb = constants[2]
        self.rg = constants[3]
        self.lp = constants[4]
        self.rp = constants[5]
        self.rc = constants[6]
        self.mb = constants[7]
        self.mp = constants[8]
        self.Ip = constants[9]
        self.b = constants[10]
        self.Ib = (2/5)*self.mb*(self.rb**2)
        
        ## A list of variables are set to the initial values
        self.q = q_init
        self.Tx = Tx
        
        ## Global variables are set to zero to be updated with the following methods
        self.M_inv = 0
        self.f = 0

    def matrixConstruction(self):
        '''
        @brief This method constructs a matrix representing a kinematic representation of the system as a function of linear and angular acceleration
        
        @details M11-M22 are the individual cell values in the 4x4 kinematic 
        matrix and are eventually added to a single matrix. The constants pushed
        to the class and the system model from Lab0x05 are used to do so The 
        inverse of the matrix is returned in preparation for creating the state 
        space system representation.
        
        @param None
        
        @returns Inverse of the constructed kinematic matrix
        '''
        ## Creates each cell in the kinematic matrix
        x = symbols('x')
        M11 = -(self.mb*(self.rb**2)+self.mb*self.rc*self.rb+self.Ib)/self.rb
        M12 = -(self.Ib*self.rb+self.Ip*self.rb+self.mb*(self.rb**3)+self.mb*self.rb*(self.rc**2)+2*self.mb*(self.rb**2)*self.rc+self.mp*self.rb*(self.rg**2)+self.mb*self.rb*(x**2))/self.rb
        M21 = -(self.mb*(self.rb**2)+self.Ib)/self.rb
        M22 = -(self.mb*(self.rb**3)+self.mb*self.rc*(self.rb**2)+self.Ib*self.rb)/self.rb
        
        ## Creates the inverse of the kinematic matrix to use in the state space representation
        M_inv = np.array(([M22,-M12],[-M21,M11]))
        M_inv = M_inv*(1/(M11*M22-M12*M21))
        return M_inv

    def stateSpace(self):
        '''
        @brief Creates a state space representation of the system model that can be used to create a time-based simulation
        
        @details Forms a vector that is the time derivative of the system model
        variables. Uses the kinematic matrix from matrixConstruction and a new
        vector from the kinetic system modeling from Lab0x05 to get the
        acceleration parameters for the ball and the platform. Returns the 
        time derivative of the state space vector, q.
        
        @param None
        
        @returns dq/dt
        '''
        ## Symbolic representations of the input parameters
        x = symbols('x')
        xdot = symbols('xdot')
        theta = symbols('theta')
        thetadot = symbols('thetadot')
        ## State space vector in symbolic representation
        q = np.array(([x],[theta],[xdot],[thetadot]))
        
        ## Symbolic representation of the motor torque
        Tx = symbols('Tx')
        
        ## Pulls the inverse kinematic matrix from matrixConstruction
        M_inv = self.matrixConstruction()
        
        ## Kinetic vector created from Lab0x05 and system parameters
        f1 = self.b*q[3]-9.81*self.mb*(q[1]*(self.rb+self.rc)+q[0])+(Tx*self.lp/self.rm)+2*self.mb*q[3]*q[0]*q[2]-9.81*self.mp*self.rg*q[1]
        f2 = -self.mb*self.rb*q[0]*(q[3]**2)-9.81*self.mb*self.rb*q[1]
        f = np.array((f1,f2))
        ## Acceleration of the ball and platform
        dq2 = M_inv@f
        
        ## Time derivative of the state space vector
        dq = np.array((q[2],q[3],dq2[0],dq2[1]))
        return dq
       
    
    def jacobian(self,large):
        '''
        @brief Linearizes the second order matrix into two first order matricies to run linear simulations
        
        @details Takes the partial derivatives of the time-derivative state space
        vector with respect to the input parameters to create two linear matricies:
        one related to the motor torque and one related to the positions and 
        velocities of the ball and platform. Returns either the 'A' matrix or
        the 'B' matrix depending on whether the large 'A' matrix is requested in
        the parameters or not
        
        @param Boolean indicating whether the Jx or Ju matrix should be returned
        
        @returns Jx (A) when large=True or Ju (B) when large=false
        '''
        ## Symbolic representations of the input parameters
        x = symbols('x')
        xdot = symbols('xdot')
        theta = symbols('theta')
        thetadot = symbols('thetadot')
        Tx = symbols('Tx')
        ## State space vector in symbolic representation
        q = np.array(([x],[theta],[xdot],[thetadot]))
        
        ## Time derivative state space vector from stateSpace method
        dq = self.stateSpace()
        
        ## Constructs empty arrays to set-up double-nested loop
        Jx = np.zeros((4,4), dtype=np.object)
        Ju = np.zeros((4,1), dtype=np.object)
        ## Double-nested loop to set up each partial derivative cell in the 
        # 4x4 and 4x1 matricies
        for i in range(0,4):
            Ju[i] = diff(dq[i],Tx)[0]
            for j in range(0,4):
                try:
                    Jx[i,j] = diff(dq[i],q[j])[0][0]
                except TypeError:
                    Jx[i,j] = diff(dq[i],q[j])[0]
            for j in range(0,4):
                Jx[i,j] = (Jx[i,j].subs([(x,self.q[0][0]),(theta,self.q[1][0]),(xdot,self.q[2][0]),(thetadot,self.q[3][0]),(Tx, self.Tx)]))
            Ju[i][0] = Ju[i][0].subs(x, self.q[0][0])
        
        ## Boolean parameter is used to determine which matrix to return
        if(large):
            return Jx
        else:
            return Ju

class Simulation():
    
    def __init__(self,A,B,q_init,Tx_init,wn,z):
        '''
        @brief Sets up class variables for the linearized jacobian vectors, system variables, and desired performance characteristics
        
        @details A and B are variables taken from the jacobian method in the 
        linearization class to run the linear simulation. q_init and Tx_init
        are the initial system variable parameters that will eventually change
        as the simulation is ran. wn and z are the desired natural frequency and
        damping coefficient of the system respectively. These values directly
        affect the controller proportional gains.
        '''
        ## Jacobian Linearized Matricies
        self.A = A
        self.B = B
        
        ## System Variables set to initial parameters
        self.q = q_init
        self.Tx = Tx_init
        
        
    def run(self,step):  
        '''
        @brief Method that runs each step of the simulation and calculates the state space variables for the next step
        
        @details Runs the linear simulation by finding the time derivative 
        state space vector using the jacobian linearized matricies and system
        variables. Then uses the Euler approximation to find the state space
        at the next point in time.
        
        @param None
        
        @returns State Space Vector
        '''
        ## Construction of the the Time-Derivative State-Space Vector using Jacobian Linearization
        Bu = (self.B)*(self.Tx)
        Ax = (self.A)@(self.q)
        qdot = Ax + Bu
        
        ## State Space Vector construction using Euler Approximation
        self.q[0] = self.q[0] + step*self.q[2]
        self.q[1] = self.q[1] + step*self.q[3]
        self.q[2] = self.q[2] + step*qdot[2]
        self.q[3] = self.q[3] + step*qdot[3]
        
        return self.q
    
    def getTx(self):
        return self.Tx
    
    def setTx(self, Tx):
        self.Tx = Tx
        
    def getA(self):
        return self.A
    
    def setA(self, A):
        self.A = A
        
    def getB(self):
        return self.B
        
    def setB(self,B):
        self.B = B
        
    def getq(self):
        return self.q
    
class Controller():
    
    def __init__(self,A,B,wn,z):
        '''
        
        '''
        ## Jacobian Linearized Matricies
        self.A = A
        self.B = B
        
         ## Desired Natrual Frequency
        self.wn = wn
        ## Desired Damping Coefficient
        self.z = z
        
        ## Two other pole values, set high to ensure system runs at desired performance
        self.a = 10*wn
        self.b = 10*wn
        
    
    def getPoles(self):
        '''
        @brief Calculates the Laplace pole representation of the system in terms of the controller gains
        
        @details Symbolically performs linear analysis of the state space 
        equation passed into the Laplace domain. Results in and returns a 
        fourth-order polynomial of the Laplace variable that can be used to 
        find the ideal system performance values and controller gain values.
        
        @param None
        
        @results Fourth-Order Laplace variable polynomial in terms of controller gain
        '''
        ## Controller gain vector
        K1 = symbols('K1')
        K2 = symbols('K2')
        K3 = symbols('K3')
        K4 = symbols('K4')
        K = np.array([[K1,K2,K3,K4]])
        
        ## Construction of Laplace State Space Equation
        BK = self.B@K
        Acl = self.A-BK
        s = symbols('s')
        sI = s*np.eye(np.size(Acl[0]))
        M = Matrix(sI-Acl)
        ## Fourth order laplace equation
        characteristic = M.det()
        return characteristic
    
    def findKs(self):
        '''
        @brief Uses the fourth-order Laplace equation in terms of the system performance parameters to find the desired controller gain values
        
        @details Calculates the fourth-order Lapace equation in terms of wn and
        z, rather than K. It then compares both equations to solve for the 
        controller gain K values in terms of the desired performance values
        
        @param None
        
        @returns Controller Gain Array
        '''
        s = symbols('s')
        ## Controller gain variables
        K1 = symbols('K1')
        K2 = symbols('K2')
        K3 = symbols('K3')
        K4 = symbols('K4')
        ## Fourth-order LaPlace transform in terms of wn and z
        c_poles = s**4 + (2*self.wn*self.z+self.a+self.b)*(s**3)+ ((self.wn**2)+(2*self.a+2*self.b)*self.wn*self.z+self.a+self.b)*(s**2)+ ((self.a+self.b)*(self.wn**2)+2*self.a*self.b*self.wn*self.z)*s + self.a*self.b*(self.wn**2)
        
        ## Fourth order Laplace transform equation in terms of controller gains, K
        k_poles = self.getPoles()
        
        ## Solve for four controller gains and add to array
        gains = solve((k_poles-c_poles), K1,K2,K3,K4, set=True)
        return gains
    
    
        
        

    
