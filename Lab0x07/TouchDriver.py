'''
@file TouchDriver.py

@brief This file contains the TouchDriver class, which controls the data collection and interpretation of the touch pad values.

@details This class takes arbatrary pin inputs set to the x and y resistances 
on the touch pad, as well as parameters for the length, width, and center point
of the touch pad. It contains three methods: two of which interpret the location
of pressure in the x and y direction respectively, and one that returns a boolean
indicating whether the touch pad is being pressed or not.

@date Created on Wed Mar 3 01:34:55 2021

@author: justin slavick
'''
import pyb
import utime

class TouchDriver:
    
    def __init__(self,xm,xp,ym,yp,length,width,center):
        '''
        @brief Initializes pins, ADC objects, and geometric parameters
        '''
        ## Pins representing resistors
        self.xm = xm
        self.xp = xp
        self.ym = ym
        self.yp = yp
        
        ## ADC object to record touchpad data
        self.adc = pyb.ADC(xm)
        
        ## Geometric Parameters to convert ADC values
        self.length = length
        self.width = width
        self.center = center
        
        ## Conversion between ADC and distance units
        self.x_scale = self.length/(3780-200)
        self.y_scale = self.width/(3625-365)
        
        ## Initial xy position set to be changed
        self.x = 0
        self.y = 0
        
    def scanX(self):
        '''
        @brief Reads the x-position of the point of contanct and converts the ADC value to a distance measurement
        
        @details Re-initializes the pins so that the touch pad will read values
        along the x-direction. Then the ADC value read is compared with the 
        touchpad ADC range, length, and center position to find the x-distance 
        in meters.
        
        @param None
        
        @returns X-position in meters
        '''
        ## Reinitializes pins and pin output voltages to read x values
        self.xm.init(mode=pyb.Pin.OUT_PP)
        self.xp.init(mode=pyb.Pin.OUT_PP)
        self.ym.init(mode=3)
        self.yp.init(mode=3)
        self.xm.low()
        self.xp.high()
        self.adc = pyb.ADC(self.ym)
        
        ## Time buffer created to allow touchpad ADC value settle before recorded
        start_time = utime.ticks_us()
        curr_time = start_time
        while(utime.ticks_diff(curr_time,start_time) <= 5):
            curr_time = utime.ticks_us()
            x_count = self.adc.read()
        
        ## Converts ADC value to distance units
        self.x = (x_count - self.center[0])*self.x_scale
        return self.x
    
    def scanY(self):
        '''
        @brief Reads the y-position of the point of contanct and converts the ADC value to a distance measurement
        
        @details Re-initializes the pins so that the touch pad will read values
        along the y-direction. Then the ADC value read is compared with the 
        touchpad ADC range, length, and center position to find the y-distance 
        in meters.
        
        @param None
        
        @returns Y-position in meters
        '''
        ## Reinitializes pins and pin output voltages to read y values
        self.ym.init(mode=pyb.Pin.OUT_PP)
        self.yp.init(mode=pyb.Pin.OUT_PP)
        self.xm.init(mode=3)
        self.xp.init(mode=3)
        self.ym.low()
        self.yp.high()
        self.adc = pyb.ADC(self.xm)
        
        ## Time buffer created to allow touchpad ADC value settle before recorded
        start_time = utime.ticks_us()
        curr_time = start_time
        while(utime.ticks_diff(curr_time,start_time) <= 5):
            curr_time = utime.ticks_us()
            y_count = self.adc.read()
        
        ## Converts ADC value to distance units
        self.y = (y_count - self.center[1])*self.y_scale
        return self.y
    
    def scanZ(self):
        '''
        @brief Reads whether the touchpad is being touched or not
        
        @details Re-initializes the pins to change pin voltage from high or low
        only when touchpad is touched. Returns a boolean indicating whether the touchpad is touched
        
        @param None
        
        @returns Boolean, is touchpad being touched?
        '''
        ## Reinitialization of pins to operate in z
        self.xm.init(mode=pyb.Pin.OUT_PP)
        self.yp.init(mode=pyb.Pin.OUT_PP)
        self.ym.init(mode=3)
        self.xp.init(mode=3)
        self.adc = pyb.ADC(self.ym)
        
        ## Time buffer created to allow touchpad ADC value settle before recorded
        start_time = utime.ticks_us()
        curr_time = start_time
        while(utime.ticks_diff(curr_time,start_time) <= 5):
            curr_time = utime.ticks_us()
            z_count = self.adc.read()
        
        ## Returns Boolean as true if ADC pin is not at max or min voltage
        return (z_count > 100 and z_count < 4000)
    
    def scan(self):
        self.x = self.scanX()
        self.y = self.scanY()
        z = self.scanZ()
        return [self.x, self.y, z]
            
        

