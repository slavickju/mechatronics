# -*- coding: utf-8 -*-
'''
@file Vendotron.py

@brief Simulates a Vending Machine that can count money, allow users to select 
drinks, and return change

@details Contains two methodds. getChange counts the amount of money in
the machine, compares the price of the drink, and returns the change.
printWelcome prints a welcome message listing the prices of each drink. This
file also contains a finite state machine that constantly updates when money
is inserted into the machine and disperses drinks or ejects change when the 
respective button is pressed

@date Created on Thu Jan 21 00:02:33 2021

@author: Justin Slavick
'''

## Defaults the money in the machine to zero
state = 0
balance = 0.00
change = 0
penny = 0
nickel = 0
dime = 0
quarter = 0
one = 0
five = 0
ten = 0
twenty = 0
cont = True

def getChange(price, payment):
    '''
    @brief Computes and returns the change upon payment of an item with a price
    
    @details Using a tuple input with data on the number of coins and dollar
    bills up to $20 as payment, this method adds up the respective value of
    each of the coins and bills. It then compares this value to the total
    price of the item requested. If adequate payment is recieved, the cost of
    the drink is subtracted from the total payment. Change is then calculated
    to give the least amount of coins and bills as possible. A tuple of the
    change is returned. If not enough payment is provided, or the tuple is too
    large, a value of None is returned.
    
    @param price The price of the item as a two decimal floating point number
    
    @param payment A tuple comprising of the number of each coin or dollar bill
    provided, in order of increasing value from pennies to twenty dollar bills.
    
    @return If funds are sufficient, returns a tuple of bills/coins. If
    insufficient or incorrect payment tuple size, returns None.
    '''
    
    total = 0
    money_distribution = (0.01,0.05,0.1,0.25,1,5,10,20)
    ## Sums payment to a total amount
    for i in range(0,8):
        total += payment[i]*money_distribution[i]
    ## Finds the change in terms of a total amount
    diff = total - price
    ## Ensures that enough payment was made
    if (diff < 0):
        return None
    else:
        ## Breaks up the change into individual dollars/coins, so that there
        ## is the least number needed for each
        for i in range(0,8):
            if i==0:
                twenty = int(diff/20)
                diff %= 20
            elif i==1:
                ten = int(diff/10)
                diff %= 10
            elif i==2:
                five = int(diff/5)
                diff %= 5
            elif i==3:
                one = int(diff)
                diff %= 1
            elif i==4:
                quarter = int(diff/0.25)
                diff %= 0.25
            elif i==5:
                dime = int(diff/0.1)
                diff %= 0.1
            elif i==6:
                nickel = int(diff/0.05)
                diff %= 0.05
            elif i==7:
                penny = int(diff/0.01)
                diff %= 0.01
                if (int(diff/0.005) >= 1):
                    penny += 1
            else:
                return None
    ## Creates the change tuple
    change = (penny, nickel, dime, quarter, one, five, ten, twenty)
    return change

def printWelcome():
    '''
    @brief Prints a welcome message that also lists the price of each drink
    
    @details As long as the balance is zero, indicating that the user is new
    to the machine or a drink was just bought, the welcome message appears.
    Each time this method is called however, the price for each drink and the
    indication toward the input that represents that drinks button is printed.
    '''
    if(balance == 0):
        print('Welcome to Volotron!')
    print(' ')
    print('Enter money or: ')
    print('C for Cuke: $1.00')
    print('P for Popsi: $1.20')
    print('S for Spryte: $0.85')
    print('D for Dr.Pupper: $1.10')
    print('E to Eject Change')
    print('Q to Quit')
    pass

while (cont):
    ## Initial State: Displays Welcome Message
    if(state == 0):
        printWelcome()
        state = 1
    ## DISPLAY_BALANCE State: Shows Current Balance
    elif(state == 1):
        print(' ')
        print('Your current balance is $' + str(round(balance, 2)))
        selection = input('')
        if(selection == 'C'):
            state = 2
        elif(selection == 'P'):
            state = 3
        elif(selection == 'S'):
            state = 4
        elif(selection == 'D'):
            state = 5
        elif(selection == 'E'):
            state = 6
        ## Quit Loop Condition
        elif(selection == 'Q'):
            cont = False
        ## Values representing each coin/bill index
        ## Represents inserting one coin or bill into machine
        elif(selection == '0'):
            penny += 1
            balance += 0.01
            print('Penny Accepted!')
        elif(selection == '1'):
            nickel += 1
            balance += 0.05
            print('Nickel Accepted!')
        elif(selection == '2'):
            dime += 1
            balance += 0.1
            print('Dime Accepted!')
        elif(selection == '3'):
            quarter += 1
            balance += 0.25
            print('Quarter Accepted!')
        elif(selection == '4'):
            one += 1
            balance += 1
            print('One Dollar Accepted!')
        elif(selection == '5'):
            five += 1
            balance += 5
            print('Five Dollars Accepted!')
        elif(selection == '6'):
            ten += 1
            balance += 10
            print('Ten Dollars Accepted!')
        elif(selection == '7'):
            twenty += 1
            balance += 20
            print('Twenty Dollars Accepted!')
        else:
            print('Not a Valid Entry')
        ## Creates tuple of payment
        change = (penny, nickel, dime, quarter, one, five, ten, twenty)
        
    ## CUKE State   
    elif(state == 2):
        change = getChange(1.00, change)
        ## Insufficient funds if change == None
        if(change != None):
            balance -= 1.00
            state = 6
            print('Cuke Dispensed!')
            print(' ')
        else:
            state = 7
        
    ## POPSI State   
    elif(state == 3):
        change = getChange(1.20, change)
        if(change != None):
            balance -= 1.20
            state = 6
            print('Popsi Dispensed!')
            print(' ')
        else:
            state = 7
        
    ## SPRYTE State  
    elif(state == 4):
        change = getChange(0.85, change)
        if(change != None):
            balance -= 0.85
            state = 6
            print('Spryte Dispensed!')
            print(' ')
        else:
            state = 7
        
    ## DR.PUPPER State
    elif(state == 5):
        change = getChange(1.10, change)
        if(change != None):
            balance -= 1.10
            state = 6
            print('Dr. Pupper Dispensed!')
            print(' ')
        else:
            state = 7
        
    ## EJECT State  
    elif(state == 6):
        print('Returned: ')
        print(str(change[0]) + ' pennies, ' + str(change[1]) + ' nickels, ' + str(change[2]) + ' dimes, ' + str(change[3]) + ' quarters, ' + str(change[4]) + ' singles, ' + str(change[5]) + ' fives, ' + str(change[6]) + ' tens, and ' + str(change[7]) + ' twenties.')
        print(' ')
        ## Resets all monetary values
        balance = 0.00
        state = 0
        penny = 0
        nickel = 0
        dime = 0
        quarter = 0
        one = 0
        five = 0
        ten = 0
        twenty = 0
        
    ## MORE_MONEY State  
    elif(state == 7):
        print('Insufficient Balance')
        ## Uses same selection value from state 1
        if(selection == 'C'):
            print('The Price for Cuke is $1.00')
        elif(selection == 'P'):
            print('The Price for Popsi is $1.20')
        elif(selection == 'S'):
            print('The Price for Spryte is $0.85')
        elif(selection == 'D'):
            print('The Price for Dr.Pupper is $1.10')
        state = 1
    else:
        pass