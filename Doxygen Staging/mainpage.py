'''
@file mainpage.py

@mainpage

@section sec_intro Introduction
This website hosts documentation related to Justin Slavick's ME405 repository located 
at:
    https://bitbucket.org/slavickju/mechatronics/src/master/
    
This portfolio contains information on the seven labs and one homework
assignment that have been completed as part of the Winter 2021 curriculum of ME
405 at Cal Poly. Below, you will find links to each of the pages representing
a brief overview of the program, links to the code sources, and references to 
each of the individual file descriptions. 

@section sec_directory Directory
1. \ref page_csh
2. \ref page_vnd
3. \ref page_rxn
4. \ref page_stp
5. \ref page_tmp
6. \ref page_mdl
7. \ref page_sim
8. \ref page_tch
9. \ref page_mtr
10. \ref page_blc

@page page_csh Cash Register (HW0x01)

@section page_csh_desc Description
This python review homework assignment computes the change required in as
little amount of coins or bills as possible given a payment in the form of a
tuple representing the numbers of each coin or bill and an item price.

@section page_csh_src Source Code Access
Source Code related to Homework0x01 is located at: https://bitbucket.org/slavickju/mechatronics/src/master/Homework/

@section page_csh_doc Documentation
Documentation related to Homework0x01 is located at \ref Homework0x01.py

@page page_vnd Vendotron (Lab0x01)

@section page_vnd_desc Description
The Vendotron is a virtual vending machine that allows the user to select one
of four drinks provided they have enough change. The method developed in
\ref page_csh is used to calculate change based on the amount of coins or bills
entering the machine and the drink selected. The machine also has the ability
to display messages and eject change with an eject button. This project helped
to develop fimiliarity with using Finite State Machines to handle multiple tasks.

@image html Slide1.JPG width=50%

@section page_vnd_src Source Code Access
Source Code related to Lab0x01 is located at: https://bitbucket.org/slavickju/mechatronics/src/master/Lab0x01/

@section page_vnd_doc Documentation
Documentation related to Lab0x02 is located at \ref Vendotron.py

@page page_rxn Think Fast! (Lab0x02)

@section page_rxn_desc Description
This lab records the average reaction time of a user that presses a button as 
soon as an LED turns on. It utilizes the callback interrupt feature in Python 
to instantaneously record the reaction time independant of the code required to 
turn on and off an LED.

@section page_rxn_src Source Code Access
Source Code related to Lab0x02 is located at: https://bitbucket.org/slavickju/mechatronics/src/master/Lab0x02/

@section page_rxn_doc Documentation
Documentation related to Lab0x02 is located at \ref ReactionTime.py

@page page_stp Pushing the Right Buttons (Lab0x03)

@section page_stp_desc Description
This lab analyzes the step response of an analog pin when increasing voltage
from 0V to 3.3V using the user interrupt. It utilizes serial communication
between the PC and Nucleo to separate the user interface and data collection
into two separate tasks.

@image html StepResponse.png width=50%

@section page_stp_src Source Code Access
Source Code related to Lab0x03 is located at: https://bitbucket.org/slavickju/mechatronics/src/master/Lab0x03/

@section page_stp_doc Documentation
Documentation related to Lab0x03 is located at \ref UI_front.py and \ref main.py

@page page_tmp Hot or Not? (Lab0x04)

@section page_tmp_desc Description
This lab was completed in collaboration with Rebecca Rodriguez. It records the
internal temperature of the STM32 microcontroller and compares it to ambient
temperature measured from a MCP9808 breakout board using I2C connection. The 
entire code runs on the Nucleo and outputs a csv file comparing the two temperatures
over time

@section page_tmp_src Source Code Access
Source Code related to Lab0x04 is located at: https://bitbucket.org/slavickju/team_8_lab0x04/src/master/

@section page_tmp_doc Documentation
Documentation related to Lab0x04 is located at \ref mcp9808.py and \ref measNucleo_temp.py

@page page_mdl Feeling Tipsy? (Lab0x05)

@section page_mdl_desc Description
This lab constists of hand calcuations of the balance platform system, in an
attempt to model its motion for the simulation and control system. It consisits
of kinematics and kinetics with four system variables and one user variable.

@image html Part1.JPG width=50%
@image html Part2.JPG width=50%
@image html Part3a.JPG width=50%
@image html Part3c.JPG width=50%

@section page_mdl_src Source Code Access
There is no source code for this lab

@section page_mdl_doc Documentation
There is no documentation for this lab

@page page_sim Simulation or Reality? (Lab0x06)

@section page_sim_desc Description
This lab takes the matrix found in lab0x05, linearizes the second-order equation
into two first-order matricies, and runs it over time with various initial
parameters. The linearization class utilizes Jacobian linearization to reduce
the matricies while the Simulation class runs the simulation. The Controller
class is used in Lab0x09

@section page_sim_1 Case 1:
@image html Case1-x.png width=50%
@image html Case1-xdot.png width=50%
@image html Case1-theta.png width=50%
@image html Case1-thetadot.png width=50%

@section page_sim_2 Case 2:
@image html Case2-x.png width=50%
@image html Case2-xdot.png width=50%
@image html Case2-theta.png width=50%
@image html Case2-thetadot.png width=50%

@section page_sim_3 Case 3:
@image html Case3-x.png width=50%
@image html Case3-xdot.png width=50%
@image html Case3-theta.png width=50%
@image html Case3-thetadot.png width=50%

@section page_sim_4 Case 4:
@image html Case4-x.png width=50%
@image html Case4-xdot.png width=50%
@image html Case4-theta.png width=50%
@image html Case4-thetadot.png width=50%

@section page_sim_cl Closed Loop:
@image html CL-pos.png width=50%
@image html CL-vel.png width=50%
@image html CL-angle.png width=50%
@image html CL-angular.png width=50%

@section page_sim_src Source Code Access
Source Code related to Lab0x06 is located at: https://bitbucket.org/slavickju/mechatronics/src/master/Lab0x06/

@section page_sim_doc Documentation
Documentation related to Lab0x04 is located at \ref openloop.py and \ref main_Lab0x06.py

@page page_tch Feeling Touchy (Lab0x07)

@section page_tch_desc Description
This lab consists of using the FSM board to read the x, y, and z positions of
a touch board. The driver contains methods to scan each of the directions
individually as well as a method to scan them all together in a tuple. The z
direction returns an boolean indicating whether an object is touching the pad
or not.

@section page_tch_setup Term Project Setup
@image html setup.JPG width=50%

@section page_tch_src Source Code Access
Source Code related to Lab0x07 is located at: https://bitbucket.org/slavickju/mechatronics/src/master/Lab0x07/

@section page_tch_doc Documentaion
Documentation related to Lab0x07 is located at \ref TouchDriver.py

@page page_mtr Term Project I (Lab0x08)

@section page_mtr_desc Description
This is the first part of the term project. This file contains both drivers for
the motors and the encoders. The motor driver regulates the speed and torque
of each of the motors while the encoder is responsible for recording its position
and angular velocity.

@section page_mtr_src Source Code Access
Source Code related to Lab0x08 is located at: https://bitbucket.org/slavickju/mechatronics/src/master/Lab0x08/

@section page_mtr_doc Documentaion
Documentation related to Lab0x08 is located at \ref MotorEncoderDriver.py

@page page_blc Term Project II (Lab0x09)

@section page_blc_desc Description
This is the final part of the term project. These files put all of the previous
four labs together to balance a ball on the balancing platform. Controller gains
are solved using linear control theory based on a desired max-peak and settling
time. The platform uses the encoders to find angles and angular velcities while
the touchpad finds positions and velocities of the ball.

@section page_blc_src Source Code Access
Source Code related to Lab0x09 is located at: https://bitbucket.org/slavickju/mechatronics/src/master/Lab0x09/

@section page_blc_doc Documentation
Documentation related to Lab0x09 is located at \ref TermProject.py and \ref main.py and \ref TouchDriver.py and \ref MotorEncoderDriver.py and \ref openloop.py
'''