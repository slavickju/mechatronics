# -*- coding: utf-8 -*-
'''
@file Homework0x01.py

@brief This code sums up coins and dollar bills and
computes change after paying for an item

@details A single method, getChange() is used to sum up the valye of all of the
individalcoins and dollar bills in a payment, compare that to the price, deduct
the price from the payment if payment is enough, and compute change in the least
number of coins and bills as possible. If payment is not enough, an error message
is returned in the method. The main code only establishes a payment tuple and 
calls and prints the getChange() method.

@date Created on Wed Jan 13 09:19:17 2021

@author: Justin Slavick
'''

def getChange(price, payment):
    '''
    @brief Counts a tuple of coins as payment and compares the total to the
    price to provide correct change.
    
    @details Using a tuple input with data on the number of coins and dollar
    bills up to $20 as payment, this method adds up the respective value of
    each of the coins and bills. It then compares this value to the total
    price of the item requested. If adequate payment is recieved, the cost of
    the drink is subtracted from the total payment. Change is then calculated
    to give the least amount of coins and bills as possible. A tuple of the
    change is returned. If not enough payment is provided, or the tuple is too
    large, a value of None is returned.
    
    @param price The price of the item as a two decimal floating point number
    
    @param payment A tuple comprising of the number of each coin or dollar bill
    provided, in order of increasing value from pennies to twenty dollar bills.
    
    @return If funds are sufficient, returns a tuple of bills/coins. If
    insufficient or incorrect payment tuple size, returns None.
    '''
    total = 0
    money_distribution = (0.01,0.05,0.1,0.25,1,5,10,20)
    ## Sums payment to a total amount
    for i in range(0,8):
        total += payment[i]*money_distribution[i]
    ## Finds the change in terms of a total amount
    diff = total - price
    ## Ensures that enough payment was made
    if (diff < 0):
        return 'Need More Money'
    else:
        ## Breaks up the change into individual dollars/coins, so that there
        ## is the least number needed for each
        for i in range(0,8):
            if i==0:
                twenty = int(diff/20)
                diff %= 20
            elif i==1:
                ten = int(diff/10)
                diff %= 10
            elif i==2:
                five = int(diff/5)
                diff %= 5
            elif i==3:
                one = int(diff)
                diff %= 1
            elif i==4:
                quarter = int(diff/0.25)
                diff %= 0.25
            elif i==5:
                dime = int(diff/0.1)
                diff %= 0.1
            elif i==6:
                nickel = int(diff/0.05)
                diff %= 0.05
            elif i==7:
                penny = int(diff/0.01)
                diff %= 0.01
                if (int(diff/0.005) >= 1):
                    penny += 1
            else:
                return None
    ## Creates the change tuple
    change = (penny, nickel, dime, quarter, one, five, ten, twenty)
    return change

## Runs from here when file is executed
if __name__ == "__main__":
    ## Payment is two twenty dollar bills. Change is computed and printed.
    payment = (0,0,0,0,0,0,0,2)
    print(getChange(21.53,payment))
        