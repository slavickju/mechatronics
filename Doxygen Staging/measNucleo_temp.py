'''
@file measNucleo_temp.py
@brief This script measures the internal temperature of the Nucleo-L47RG and saves it to a file.
@author Justin Slavick and Rebecca Rodriguez
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
@date February 7, 2021
'''

import pyb 
import utime
# import mcp9808.py
from pyb import UART

## In order to separate the task of reading the internal temperature with the microcontroller with the task of writing the data to the CSV file, this class is created to do the later.
class TempWriter:
    
    ## Constant defining State 0 - Initialization
    S0_INIT        = 0    
    
    ## Constant defining State 1
    S1_GET_INTERNAL_TEMP  = 1  
    
    def __init__(self, interval, uart, end_time):
        '''
        

        Parameters
        ----------
        interval : TYPE
            DESCRIPTION.
        uart : TYPE
            DESCRIPTION.

        Returns
        -------
        None.

        '''
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
           
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)
        
        ## The timestamp for the first iteration / Time in microseconds
        self.start_time = utime.ticks_ms()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        self.end_time = end_time
        
        ## Sets up UART to send CSV file to computer
        self. myuart = UART(uart)
        
        ## Constructs an I2C object to be used by the Nucleo as a Master
        self.i2c1_master = pyb.I2C(1, pyb.I2C.MASTER, baudrate = 100000)
        
        ## Constructs an I2C object to be used by the mcp9808 board as a slave
        ## ***The address of 18 is temporary until I figure out how to access the real address***
        self.i2c1_slave = pyb.I2C(1, pyb.I2C.SLAVE, addr=18, baudrate = 100000)
        
        ## Constructs a TaskAmbientTemp object which recieves the I2C object and its address. This allows the ambient temperature from the mcp9808 device to be measured.
        ## ***The address of 18 is temporary until I figure out how to access the real address***
        # self.mcp_temp = mcp9808.TaskAmbientTemp(60000, i2c1_slave, 18)
        
        ## Constructs a TaskInternalTemp object that allows the internal temperature of the microcontroller to be measured.
        self.nucleo_temp = TaskInternalTemp()
        
        self.ambient_temp_list = []
        self.internal_temp_list = []
        
    def run(self):
        '''
        @param ______.
        '''
        
        with open ("tempdata.csv","w") as a_file:
            a_file.write ("Time(s),STM32 Temperature(C),Ambient (MSP9808) Temperature\r")
            # Add temperatures of Nucleo Board and mcp9808 to list every 60 sec.
            while(True):
                try:
                    self.curr_time = utime.ticks_ms()
                    if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
                        time = utime.ticks_diff(self.curr_time,self.start_time)/1000
                        internal_temp = self.nucleo_temp.celsius()
                        ambient_temp = self.mcp_temp.celsius()
                        a_file.write ("{t},{i},{a}\r".format (t=time, i=internal_temp, a=ambient_temp))
                        ## Specifying the next time the task will run
                        self.next_time = utime.ticks_add(self.next_time, self.interval)
                except KeyboardInterrupt:
                    break
            print ("The file has by now automatically been closed.")

class TaskInternalTemp:
    '''
    
    '''
    
    def __init__(self):
        '''
        Creates an internal temperature task object.
        @param interval An integer number of microseconds between desired runs of the task
        @param dbg A boolean indicating whether the task should print a trace or not
        '''
        
        self.intData = []
        self.adcall = pyb.ADCAll(12, 0x70000) # 12 bit resolution, internal channels
        
        self.temp = 0;
        
        pass
        
    def celsius(self):
        self.adcall.read_vref()
        self.temp = self.adcall.read_core_temp()
        return self.temp
    
    def fahrenheit(self):
        self.temp = self.adcall.read_core_temp()
        return self.temp * (9/5) + 32
       
if __name__ == "__main__":
    write = TempWriter(1000,2,30000)
    write.run()