'''
@file measNucleo_temp.py
@brief This script measures the internal temperature of the Nucleo-L47RG and saves it to a file.
@author Justin Slavick and Rebecca Rodriguez
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
@date February 7, 2021
'''

import pyb 
import utime

class TaskInternalTemp:
    '''
    
    '''

    ## Constant defining State 0 - Initialization
    S0_INIT        = 0    
    
    ## Constant defining State 1
    S1_GET_INTERNAL_TEMP  = 1  
    
    def __init__(self, interval):
        '''
        Creates an internal temperature task object.
        @param interval An integer number of microseconds between desired runs of the task
        @param dbg A boolean indicating whether the task should print a trace or not
        '''
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
           
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)
        
        ## The timestamp for the first iteration / Time in microseconds
        self.start_time = utime.ticks_ms()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        self.intData = []
        self.adcall = pyb.ADCAll(12, 0x70000) # 12 bit resolution, internal channels

        pass
    
    def run(self):
        '''
        @param ______.
        '''
        # Check internal temp of Nucleo board every 60 sec.
        
        self.temp = self.adcall.read_core_temp()
        
        self.intTemp = self.adcall.read_core_temp()
        
        return self.intTemp