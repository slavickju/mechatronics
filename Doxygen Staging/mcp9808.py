'''
@file mcp9808.py
@brief This program allows the user to communicate with the temperature sensor - MCP9808 via microcontroller using I2C. The temperature sensor 
       is powered by 3.3 V from the microcontroller and the I2C communication wires are connected to the SCL pin and SDA pin. 
@author Justin Slavick and Rebecca Rodriguez
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
@date February 7, 2021
'''
import pyb
from pyb import I2C
import utime

class TaskAmbientTemp:
    '''
    This class establishes I2C communication between the temperature module and microcontroller.
    '''

	## Constructor for encoder task
	#
	#  Detailed info on encoder task constructor
    ## Constant defining State 0 - Initialization
    S0_INIT        = 0    
    
    ## Constant defining State 1
    S1_GET_UPDATE  = 1  
    
    def __init__(self, i2c1, address):
        '''
        Creates an ambient temperature task object.
        @param taskNum A number to identify the task
        @param interval An integer number of microseconds between desired runs of the task
        @param dbg A boolean indicating whether the task should print a trace or not
        '''
        
        # Create a buffer for data storage
        self.ambData = bytearray()
        
        self.i2c1 = i2c1
        
        self.address = address
        
        self.tempC = 0
        
        # Check for start condition ()
        pass
    
    def check(self):
        '''
        Verifies the sensor is attached at the same bus address according to the value in the manufacturer ID register.
        @param ______.
        '''
        # Scans all I2C addresses from 0x01 to 0x7f & returns a list of those that respond.
        ID_list = self.i2c1.mem_read (2, self.address, 0x06)   # Read sensor data
        ## From power-on values in mcp9808 data sheet:
        return (ID_list[0] == 0 and ID_list[1] == 84)
    
    def decodeTemp(self, tempBytes):
        '''
        

        Parameters
        ----------
        tempBytes : TYPE
            DESCRIPTION.

        Returns
        -------
        temp : TYPE
            DESCRIPTION.

        '''
        ## The Breakout Board returns a byte array with two indices. The first index represents the most significant byte while the second represents the least significant byte
        MSB = tempBytes[0]
        LSB = tempBytes[1]
        
        ## Initial temperature is set to 0. i represents the bit position and is also set to 0
        temp = 0
        i = 0
        ## While loop will keep right bit shifting LSB until it runs out of ones
        while (LSB != 0):
            ## The right-most bit is compared with a binary value of 1. That value of one or zero is multiplied with the resolution of that bit and added to the total temperature
            temp += (LSB & 0b1)*(2**(i-4))
            LSB = LSB>>1
            i += 1
        
        ## The bit counter is reset and the process is repeated with the MSB, until the bit representing the sign is reached
        i = 0
        while (i <= 3):
            temp += (MSB & 0b1)*(2**(i+4))
            MSB = MSB>>1
            i += 1
            
        ## If the bit representing the sign is true, the value of temp is multiplied by -1.
        if(MSB>>1 == 1):
            temp *= -1
            
        return temp
    
    def celsius(self):
        '''
        Returns measured temp in celsius.
        @param mcpTemp Represents the ambient temperature recorded by the MCP9808 module.
        '''
        ## Needs if statements to decode output to decimal form
        
        
        tempBytes = self.i2c1.mem_read (2, self.address, 0x05)   # Read sensor data
        self.tempC = self.decodeTemp(tempBytes)
        return self.tempC
    
    def fahrenheit(self, tempC):
        '''
        Returns measured temp in fahrenheit.
        @param mcpTemp Represents the ambient temperature recorded by the MCP9808 module.
        '''
        # Convert Celsius temp to Fahrenheit
        return ((tempC * (9 / 5)) + 32)
    
    
if __name__ == "__main__":
    # Verify that temp can be recorded once per second
    i2c1 = pyb.I2C (1, pyb.I2C.MASTER, baudrate = 100000)
    
    # Scans all I2C addresses from 0x01 to 0x7f & returns a list of those that respond.
    address_list = i2c1.scan()                                         # Check for devices on the bus
    try:
        address = address_list[0]
        print(address)
    except IndexError:
        print('IndexError')
        address = 24
    
    
    mcp = TaskAmbientTemp(i2c1, address)
    
    
    if(mcp.check()):
        ##  The amount of time in microseconds between runs of the task
        interval = 1000
        
        ## The timestamp for the first iteration / Time in microseconds
        start_time = utime.ticks_ms()
        
        ## The "timestamp" for when the task should run next
        next_time = utime.ticks_add(start_time, interval)
        while(True):
            try:
                curr_time = utime.ticks_ms()
                if utime.ticks_diff(curr_time, next_time) >= 0:
                    print(mcp.celsius())
                    # byte = mcp.celsius()
                    # temp = byte[0]
                    
                    ## Specifying the next time the task will run
                    next_time = utime.ticks_add(next_time, interval)
            except KeyboardInterrupt:
                break
    else:
        print('Incorrect ID')