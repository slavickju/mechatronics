# -*- coding: utf-8 -*-
"""
Created on Thu Feb 11 01:16:27 2021

@author: justi
"""
LSB = 131
MSB = 193

temp = 0
i = 0
while (LSB != 0):
    temp += (LSB & 0b1)*(2**(i-4))
    LSB = LSB>>1
    i += 1
    
i = 0
while (i <= 3):
    temp += (MSB & 0b1)*(2**(i+4))
    MSB = MSB>>1
    i += 1
    
if(MSB>>1 == 1):
    temp *= -1
    
print(temp)