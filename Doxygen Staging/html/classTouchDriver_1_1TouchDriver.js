var classTouchDriver_1_1TouchDriver =
[
    [ "__init__", "classTouchDriver_1_1TouchDriver.html#a9035e4908400e98b4cf7482bebea6056", null ],
    [ "scan", "classTouchDriver_1_1TouchDriver.html#a249dbd860caabdb72d2918691add58f0", null ],
    [ "scanX", "classTouchDriver_1_1TouchDriver.html#a7fbb2789da86f6268630dac564bd8461", null ],
    [ "scanY", "classTouchDriver_1_1TouchDriver.html#a0049ad1ca0bb8311c7784d760eefbdb1", null ],
    [ "scanZ", "classTouchDriver_1_1TouchDriver.html#aed031150140095d897b9ac4f0e669c0f", null ],
    [ "adc", "classTouchDriver_1_1TouchDriver.html#a7a0fbb2a2536b715d088aafe5ea0ea60", null ],
    [ "center", "classTouchDriver_1_1TouchDriver.html#a6abe8a1b39801c30e54257b4383a8b5a", null ],
    [ "length", "classTouchDriver_1_1TouchDriver.html#a057fe67190a21bf2c21384130fb3d59b", null ],
    [ "width", "classTouchDriver_1_1TouchDriver.html#a5fc3cd8bd5637a88054288ef0711bc3b", null ],
    [ "x", "classTouchDriver_1_1TouchDriver.html#a50c09994b240a2a5c1f522bb2d27f8b8", null ],
    [ "x_scale", "classTouchDriver_1_1TouchDriver.html#a85bbf3d9aa363a5ad4257567d2a7dd6a", null ],
    [ "xm", "classTouchDriver_1_1TouchDriver.html#ad2bcfb9458b3796a969dd2e2b64b56d9", null ],
    [ "xp", "classTouchDriver_1_1TouchDriver.html#a761d121772a8eec2757a1cd4e2933291", null ],
    [ "y", "classTouchDriver_1_1TouchDriver.html#a9f7a954f0de7ddb15bd933b28f3d160a", null ],
    [ "y_scale", "classTouchDriver_1_1TouchDriver.html#ad634c2fbb58fd37ca32cb8dc56afb075", null ],
    [ "ym", "classTouchDriver_1_1TouchDriver.html#a37563134ee4a571827b3ceab6388f509", null ],
    [ "yp", "classTouchDriver_1_1TouchDriver.html#a62d7de472b167f08cd66899753297254", null ]
];