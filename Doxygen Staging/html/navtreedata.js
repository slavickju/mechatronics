/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "ME 405 Documentation Portfolio", "index.html", [
    [ "Introduction", "index.html#sec_intro", null ],
    [ "Directory", "index.html#sec_directory", null ],
    [ "Cash Register (HW0x01)", "page_csh.html", [
      [ "Description", "page_csh.html#page_csh_desc", null ],
      [ "Source Code Access", "page_csh.html#page_csh_src", null ],
      [ "Documentation", "page_csh.html#page_csh_doc", null ]
    ] ],
    [ "Vendotron (Lab0x01)", "page_vnd.html", [
      [ "Description", "page_vnd.html#page_vnd_desc", null ],
      [ "Source Code Access", "page_vnd.html#page_vnd_src", null ],
      [ "Documentation", "page_vnd.html#page_vnd_doc", null ]
    ] ],
    [ "Think Fast! (Lab0x02)", "page_rxn.html", [
      [ "Description", "page_rxn.html#page_rxn_desc", null ],
      [ "Source Code Access", "page_rxn.html#page_rxn_src", null ],
      [ "Documentation", "page_rxn.html#page_rxn_doc", null ]
    ] ],
    [ "Pushing the Right Buttons (Lab0x03)", "page_stp.html", [
      [ "Description", "page_stp.html#page_stp_desc", null ],
      [ "Source Code Access", "page_stp.html#page_stp_src", null ],
      [ "Documentation", "page_stp.html#page_stp_doc", null ]
    ] ],
    [ "Hot or Not? (Lab0x04)", "page_tmp.html", [
      [ "Description", "page_tmp.html#page_tmp_desc", null ],
      [ "Source Code Access", "page_tmp.html#page_tmp_src", null ],
      [ "Documentation", "page_tmp.html#page_tmp_doc", null ]
    ] ],
    [ "Feeling Tipsy? (Lab0x05)", "page_mdl.html", [
      [ "Description", "page_mdl.html#page_mdl_desc", null ],
      [ "Source Code Access", "page_mdl.html#page_mdl_src", null ],
      [ "Documentation", "page_mdl.html#page_mdl_doc", null ]
    ] ],
    [ "Simulation or Reality? (Lab0x06)", "page_sim.html", [
      [ "Description", "page_sim.html#page_sim_desc", null ],
      [ "Case 1:", "page_sim.html#page_sim_1", null ],
      [ "Case 2:", "page_sim.html#page_sim_2", null ],
      [ "Case 3:", "page_sim.html#page_sim_3", null ],
      [ "Case 4:", "page_sim.html#page_sim_4", null ],
      [ "Closed Loop:", "page_sim.html#page_sim_cl", null ],
      [ "Source Code Access", "page_sim.html#page_sim_src", null ],
      [ "Documentation", "page_sim.html#page_sim_doc", null ]
    ] ],
    [ "Feeling Touchy (Lab0x07)", "page_tch.html", [
      [ "Description", "page_tch.html#page_tch_desc", null ],
      [ "Term Project Setup", "page_tch.html#page_tch_setup", null ],
      [ "Source Code Access", "page_tch.html#page_tch_src", null ],
      [ "Documentaion", "page_tch.html#page_tch_doc", null ]
    ] ],
    [ "Term Project I (Lab0x08)", "page_mtr.html", [
      [ "Description", "page_mtr.html#page_mtr_desc", null ],
      [ "Source Code Access", "page_mtr.html#page_mtr_src", null ],
      [ "Documentaion", "page_mtr.html#page_mtr_doc", null ]
    ] ],
    [ "Term Project II (Lab0x09)", "page_blc.html", [
      [ "Description", "page_blc.html#page_blc_desc", null ],
      [ "Source Code Access", "page_blc.html#page_blc_src", null ],
      [ "Documentation", "page_blc.html#page_blc_doc", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"",
"main_8py.html#a362f6580dcbc32106ac5bc63af7c4682"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';