var files_dup =
[
    [ "Homework0x01.py", "Homework0x01_8py.html", "Homework0x01_8py" ],
    [ "main.py", "main_8py.html", "main_8py" ],
    [ "main_Lab0x06.py", "main__Lab0x06_8py.html", "main__Lab0x06_8py" ],
    [ "mcp9808.py", "mcp9808_8py.html", "mcp9808_8py" ],
    [ "measNucleo_temp.py", "measNucleo__temp_8py.html", "measNucleo__temp_8py" ],
    [ "MotorEncoderDriver.py", "MotorEncoderDriver_8py.html", "MotorEncoderDriver_8py" ],
    [ "openloop.py", "openloop_8py.html", [
      [ "Linearization", "classopenloop_1_1Linearization.html", "classopenloop_1_1Linearization" ],
      [ "Simulation", "classopenloop_1_1Simulation.html", "classopenloop_1_1Simulation" ],
      [ "Controller", "classopenloop_1_1Controller.html", "classopenloop_1_1Controller" ]
    ] ],
    [ "ReactionTime.py", "ReactionTime_8py.html", "ReactionTime_8py" ],
    [ "TermProject.py", "TermProject_8py.html", "TermProject_8py" ],
    [ "TouchDriver.py", "TouchDriver_8py.html", [
      [ "TouchDriver", "classTouchDriver_1_1TouchDriver.html", "classTouchDriver_1_1TouchDriver" ]
    ] ],
    [ "UI_front.py", "UI__front_8py.html", "UI__front_8py" ],
    [ "Vendotron.py", "Vendotron_8py.html", "Vendotron_8py" ]
];