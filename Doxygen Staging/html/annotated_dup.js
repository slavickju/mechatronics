var annotated_dup =
[
    [ "mcp9808", null, [
      [ "TaskAmbientTemp", "classmcp9808_1_1TaskAmbientTemp.html", "classmcp9808_1_1TaskAmbientTemp" ]
    ] ],
    [ "measNucleo_temp", null, [
      [ "TaskInternalTemp", "classmeasNucleo__temp_1_1TaskInternalTemp.html", "classmeasNucleo__temp_1_1TaskInternalTemp" ],
      [ "TempWriter", "classmeasNucleo__temp_1_1TempWriter.html", "classmeasNucleo__temp_1_1TempWriter" ]
    ] ],
    [ "MotorEncoderDriver", null, [
      [ "EncoderDriver", "classMotorEncoderDriver_1_1EncoderDriver.html", "classMotorEncoderDriver_1_1EncoderDriver" ],
      [ "MotorDriver", "classMotorEncoderDriver_1_1MotorDriver.html", "classMotorEncoderDriver_1_1MotorDriver" ]
    ] ],
    [ "openloop", null, [
      [ "Controller", "classopenloop_1_1Controller.html", "classopenloop_1_1Controller" ],
      [ "Linearization", "classopenloop_1_1Linearization.html", "classopenloop_1_1Linearization" ],
      [ "Simulation", "classopenloop_1_1Simulation.html", "classopenloop_1_1Simulation" ]
    ] ],
    [ "TouchDriver", null, [
      [ "TouchDriver", "classTouchDriver_1_1TouchDriver.html", "classTouchDriver_1_1TouchDriver" ]
    ] ]
];