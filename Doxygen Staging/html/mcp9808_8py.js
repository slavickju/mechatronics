var mcp9808_8py =
[
    [ "TaskAmbientTemp", "classmcp9808_1_1TaskAmbientTemp.html", "classmcp9808_1_1TaskAmbientTemp" ],
    [ "address", "mcp9808_8py.html#a089290bc0c9e4923b9c6d0b9368eddd3", null ],
    [ "address_list", "mcp9808_8py.html#abad0bdc2397285f02fe39f82176422fb", null ],
    [ "curr_time", "mcp9808_8py.html#aed2ee2a142a69c09ac28a97144ae52b9", null ],
    [ "i2c1", "mcp9808_8py.html#addb606b1241a8de4c32db55032f8fb3d", null ],
    [ "interval", "mcp9808_8py.html#a0269c0c2162adffcf10233cf6c857dff", null ],
    [ "mcp", "mcp9808_8py.html#a54383af2693144482bf8fc0aa86883b1", null ],
    [ "next_time", "mcp9808_8py.html#a4ce9b3397008a94f9b67dfbf65ccf7b7", null ],
    [ "start_time", "mcp9808_8py.html#a19bbffa38ced2b2a70db242e5c5c430c", null ]
];