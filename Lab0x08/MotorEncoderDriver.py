'''
@file MotorEncoderDriver.py

@brief This file contains the driver classes to run the motors and the encoders

@details Contains MotorDriver and EncoderDriver. Both classes initialize with 
arbatrary pins signalizing the motor/encoder pins, the timers, and the channels
needed to operate the equipment. The duty cycle can be changed and updated on
the motors while the encoders have an update method that takes bit skipping
into account.

@date Created on Wed Mar 10 14:50:01 2021

@author justin slavick
'''
import pyb
import utime

class EncoderDriver:  

    def __init__(self,pin1,pin2,timer):
        '''
        @brief Creates a timer in encoder mode.
        
        @details Inputs two pin locations in the form of a string and a timer 
        in the form of an integer. This method uses these values to set up a 
        Timer in encoder mode using the specified int timer value. Also
        creates two stored_timer variables that store the two most recent
        updated timer counter values. There are two in order to compare the 
        delta and prevent overflow and underflow.
        '''
        
        self.tim = timer
        self.tch1 = self.tim.channel(1, pin=pin1, mode=pyb.Timer.ENC_AB)
        self.tch2 = self.tim.channel(2, pin=pin2, mode=pyb.Timer.ENC_AB)
        self.stored_timer1 = 0
        self.stored_timer2 = 0
        self.position = 0
        
        self.PPR = 0
        
    def update(self):
        '''
        @brief Updates the encoder's position and corrects for over/underflow.
        
        @details Changes the value of stored_timer to that currently outputted
        by the pyb Timer class's counter() method. Also checks for overflow
        or underflow. If the magnetude of delta is greater than half the 
        period, it corrects the value by adding or subtracting the period,
        depending on if delta is positive or negative.
        '''
        self.stored_timer2 = self.stored_timer1
        self.stored_timer1 = self.tim.counter()
        if (abs(self.get_delta()) > self.PPR/2):
            if(self.get_delta() < -self.PPR/2):
                self.position = self.position + self.set_delta(self.PPR)
            else:
                self.position = self.position + self.set_delta(-self.PPR)
        else:
            self.position = self.position + self.get_delta()
        
    def get_position(self):
        '''
        @brief Gets the encoder's most recent stored position.
        
        @details Returns the value of stored_timer1.
        '''
        # return self.position
        return self.position

    def set_position(self, position):
        '''
        @brief Changes the encoder's position and updates the stored_encoder
        value
        
        @details Changes the value of pyb Timer's counter method to the value 
        given in position. It then calls the update method to update the
        stired timer value. Mostly used to set the encoder position to zero.
        It also resets both stored_timer values to avoid outputting a delta
        due to recalibration.
        '''
        self.tim.counter(position)
        self.stored_timer1 = 0
        self.stored_timer2 = 0
        self.update()
        
    def get_delta(self):
        '''
        @brief Returns the difference between the two most recent stored
        timer values
        
        @details Finds the difference between stored_timer1 and stored_timer2
        in order to find if there is. overflow or underflow in the encoder.
        This value is returned
        '''
        return (self.stored_timer1 - self.stored_timer2)
    
    def set_delta(self, value):
        '''
        @brief Changes the delta to a different value
        
        @details Returns delta added with another value initialized in the 
        method. The main use of this is to correct "bad" delta values by
        adding or subtracting the period.
        '''
        return (self.get_delta() + value)
    
    def set_PPR(self, PPR):
        self.PPR = PPR
        
        
class MotorDriver:
    ''' 
    This class implements a motor driver for the ME405 board. 
    '''    
    def __init__ (self, nSLEEP_pin, IN1_pin, IN2_pin, channel1, channel2, timer):
        ''' 
        Creates a motor driver by initializing GPIO12pins and turning the motor 
        off for safety.
        
        @param nSLEEP_pin   A pyb.Pin object to use as the enable pin.
        @param IN1_pin      A pyb.Pin object to use as the input to half bridge 1.
        @param IN2_pin      A pyb.Pin object to use as the input to half bridge 2.
        @param timer        A pyb.Timer object to use for PWM generation on
        IN1_pin and IN2_pin. 
        '''
        # print ('Creating a motor driver')
        self.sleep = nSLEEP_pin
        self.in1 = IN1_pin
        self.in2 = IN2_pin
        self.tim3 = timer
        self.t3ch1 = self.tim3.channel(channel1, pyb.Timer.PWM, pin=self.in1)
        self.t3ch2 = self.tim3.channel(channel2, pyb.Timer.PWM, pin=self.in2)
        self.duty = 0
        
    
    def enable (self):
        # print ('Enabling Motor')
        self.sleep.high()
        
    def disable (self):
        # print ('Disabling Motor')
        self.sleep.low()
        self.in1.low()
        self.in2.low()
        
    def set_duty (self, duty):
        ''' 
        This method sets the duty cycle to be sent to the motor to the given 
        level. Positive values cause effort in one direction, negative values 
        in the opposite direction.
        
        @param duty A signed integer holding the duty cycle of the PWM signal 
        sent to the motor 
        '''
        self.duty = duty
        if (duty<=0):
            self.in2.low()
            self.t3ch1.pulse_width_percent(abs(self.duty))
        else:
            self.in1.low()
            self.t3ch2.pulse_width_percent(abs(self.duty))
        
        
    def get_duty (self):
        ''' 
        This method sets the duty cycle to be sent to the motor to the given 
        level. Positive values cause effort in one direction, negative values 
        in the opposite direction.
        
        @param duty A signed integer holding the duty cycle of the PWM signal 
        sent to the motor 
        '''
        return self.duty
    
    
if (__name__ =='__main__'):
    # Adjust the following code to write a test program for your motor 
    # class. Any code within the if __name__ == '__main__' block will 
    # only run when the script is executed as a standalone program. If 
    # the script is imported as a module the code block will not run.
            
    # Create the pin objects used for interfacing with the motor driver
    sleep = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
    in1 = pyb.Pin(pyb.Pin.cpu.B4, pyb.Pin.OUT_PP)
    in2 = pyb.Pin(pyb.Pin.cpu.B5, pyb.Pin.OUT_PP)
    tim3 = pyb.Timer(3, freq=20000)
    
    # # Create a motor object passing in the pins and timer
    # moe = MotorDriver(sleep,in1,in2,1,2,tim3)
    
    # # Enable the motor driver
    # moe.enable()
    
    # # Set the duty cycle to 10 percent
    # moe.set_duty(50, True)
    
    ## EncoderDriver Pins and Parameters
    e1ch1 = pyb.Pin(pyb.Pin.cpu.B6)
    e1ch2 = pyb.Pin(pyb.Pin.cpu.B7)
    e2ch1 = pyb.Pin(pyb.Pin.cpu.C6)
    e2ch2 = pyb.Pin(pyb.Pin.cpu.C7)
    timer1 = 4
    timer2 = 8
    tim1 = pyb.Timer(timer1, freq=20000)
    tim2 = pyb.Timer(timer2, freq=20000)
    t4ch1 = tim1.channel(1, pin=e1ch1, mode=pyb.Timer.ENC_AB)
    t4ch2 = tim1.channel(2, pin=e1ch2, mode=pyb.Timer.ENC_AB)
    t8ch1 = tim2.channel(1, pin=e2ch1, mode=pyb.Timer.ENC_AB)
    t8ch2 = tim2.channel(2, pin=e2ch2, mode=pyb.Timer.ENC_AB)
    
    
    encoder_y = EncoderDriver(e1ch1,e1ch2,tim1)
    encoder_x = EncoderDriver(e2ch1,e2ch2,tim2)
    ## Set PPR of Encoders
    PPR = 4000
    encoder_x.set_PPR(PPR)
    encoder_y.set_PPR(PPR)

    ## Positions Set Based On Initial Parameters
    encoder_x.set_position(0)
    encoder_y.set_position(0)
    
    while(True):
        # print(tim1.counter())
        encoder_y.update()
        print(encoder_y.get_position())