# -*- coding: utf-8 -*-
'''
@file ReactionTime.py

@brief This program allows the user to test their reaction time by hitting a
button once an LED lights up. It utilizes callback interrupts to record time
instantaneously. Once the program is ended, the average reaction time and
total number of tries is displayed.

@details The objective of this lab is to utilize the callback interrupt feature.
This program runs directly on the Nucleo REPL. After constructing the objects
for the LED, user pin, and timer, a while loop runs constantly looking for the
KeyboardInterrupt that will end the program. Meanwhile, it picks a random number
between two and three seconds to turn the LED on once its detected to be off. 
After turning on, it waits another second before turning off again. The callback
method acts as the interrupt condition that occurs when a falling edge is
detected on the button. It also records reaction time and turns the LED off,
separately from the while loop and instantaneously. Once the KeyboardInterrupt
is detected, each item in the list of reaction times is added and the average
reaction time and total number of tries is displayed.

@date Created on Wed Jan 27 17:33:03 2021

@author Justin Slavick
'''

# Import any required modules
import pyb
import micropython
import utime
import random
# Create emergency buffer to store errors that are thrown
# inside ISR
micropython.alloc_emergency_exception_buf(200)

## Pin object to use for blinking LED. Attached to PA5
myPin = pyb.Pin(pyb.Pin.cpu.A5, mode=pyb.Pin.OUT_PP)
## Pin object to use for user button. Attached to PC13
myInterrupt = pyb.Pin(pyb.Pin.cpu.C13, mode=pyb.Pin.IN)   

## Create a timer object (timer 6) in general purpose counting mode
#  at 10Hz
myTimer = pyb.Timer(2, prescaler=79, period=0x7FFFFFFF)

reaction = []
tries = 0
total = 0

def callback(line):
    '''
    @brief Interrupts the running code and records the reaction time
    
    @detailed This is the callback interrupt method that runs instantaneously
    as soon as the user pin PC13 enters a falling edge. This method occurs
    independant of any other code running, including delays. It does not have
    a function when the LED light is off, indicating too early of a reaction
    time. If the light is turned on, it adds the timer counter value,
    indicating the reaction time in microseconds, to a list. It also turns the
    LED off to indicate that the button has been pressed
    
    @param line This parameter is one that is needed to call the method. It
    does not have a use in this class
    '''
    if (myPin.value() == 1):
        reaction.append(myTimer.counter() // 1000)
        myPin.value(0)
    
    
## Create a extint object to create a callback when user pin is pressed
extint = pyb.ExtInt(myInterrupt, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, callback)

## Main program loop, starts by turning off the LED if on
myPin.value(0)
while True:
    ## Exception will be called when ctrl+c is entered, allowing for reaction
    # time stats to be displayed
    try:
        ## If the LED is detected to be off, a random amount of time between 2
        # and 3 seconds is delayed before the LED is turned on. The number of
        # tries is increased and the timer is restarted here.
        if (myPin.value() == 0):
            utime.sleep_us(2000000+random.randrange(1000000))
            tries += 1
            myPin.value(1)
            myTimer.counter(0)
        ##If after 1 million microseconds of the timer being on there is no
        # interrupt, the LED is turned off and 1000 milliseconds is added to 
        # a list of reaction times
        if (myTimer.counter() >= 1000000):
            reaction.append(1000)
            myPin.value(0)
    except KeyboardInterrupt:
        ## The list of reaction times is iterated to calculate the total
        # reaction time. This value is divided by the number of tries (corrected
        # to ignore the try value added before the error) to calculate the 
        # average reaction time. Average reaction time and total number of tries
        # is displayed before the while loop is forcibly ended and the LED is
        # turned off
        for n in range(0,tries-1):
            total += reaction[n]
        print('Average Reaction Time = ' + str(total/(tries-1)) +'ms')
        print('Number of Tries = ' + str(tries-1))
        myPin.value(0)
        break