# -*- coding: utf-8 -*-
'''
@file main.py

@brief This task collects the step response data from the ADC

@details This task utilizes UART to communicate with the UI FrontEnd. It waits
to read a value from a user to initialize data collection. When the user button
is pressed after that, it interrupts the task to turn on an analog pin to 3.3V
and records the step response of the voltage at 10Hz intervals. After 
componding that data to a batch file, it writes the data for the file onto its 
REPL for the PC to read and collect.

@date Created on Wed Feb  3 21:52:31 2021

@author: justin slavick
'''

# Import any required modules
import pyb
import micropython
from pyb import UART
import array

myuart = UART(2)


# Create emergency buffer to store errors that are thrown
# inside ISR
micropython.alloc_emergency_exception_buf(200)

## Pin object to use for user button. Attached to PC13
myUser = pyb.Pin(pyb.Pin.cpu.C13, mode=pyb.Pin.IN)

## Pin object to use for the analog voltage output pin. Attached to PA0
myADCPin= pyb.Pin(pyb.Pin.board.PA0)

## Create a timer object (timer 6) in general purpose counting mode
#  at 10Hz. Will be used to record voltage data quickly on pyb.ADC's read_timed
# method.
myTimer = pyb.Timer(2, freq=100000)

## Creates the buffer in the form of an array. This allows all 12 bits to be
# sent to the PC as a batch file.
buffy = array.array ('H', (0 for index in range (200)))

## Creates the object used to read the voltage value of the analog pin
myADCReader = pyb.ADC(pyb.Pin.board.PA0)


def callback(line):
    '''
    @brief Interrupts the running code and records the analog voltage step response
    
    @detailed This is the callback interrupt method that runs instantaneously
    as soon as the user pin PC13 enters a falling edge. This method occurs
    independant of any other code running, including delays. It does not have
    a function when input to record data is not recieved. If the input is
    recieved, it sets the analog voltage to 3.3V, records the step response
    using a timed read method, and writes the batch file created to the PC.
    
    @param line This parameter is one that is needed to call the method. It
    does not have a use in this class
    '''
    ## Takes quick and repeated readings of the analog pin voltage value
    # If readings indicate that the pin is at 0V, that means the button is still
    # pushed and readings are occuring too frequently. Only when the read()
    # method returns a value above 10 will an accurate step response be recorded.
    # The read_timed() method is used to allocate the step response quickly to
    # a buffer array.
    while(myADCReader.read() <= 10):
        pass
    myADCReader.read_timed(buffy,myTimer)
    
    
    

## Boolean to prevent user button from interrupting task before data has been
# requested
no_exint = True
while (buffy[0] == 0):
    try:
        ## Waits to see if there is a character in the REPL before running code
        if myuart.any() != 0:
            val = myuart.readchar()
            if(val == 0x47 and no_exint):
                ## Create a extint object to create a callback when user pin is pressed
                extint = pyb.ExtInt(myUser, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, callback)
                no_exint = False
    except KeyboardInterrupt: 
        ## Breaks while loop when ctrl+c is inputted in the Nucleo REPL
        break
       
## Writes the string of voltage values to the PC to be analyzed
for index in range(200):
    myuart.write(str(buffy[index])+',')
        
            
 
                
                
                
                
                
                
                