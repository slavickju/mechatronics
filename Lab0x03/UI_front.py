# -*- coding: utf-8 -*-
'''
@file UI_front.py

@brief Allows the User to input commands and read data from the Nucleo

@details This file utilizes pySerial in order to write characters into ascii
onto the Nucleo Development Board. After writing the input command, it waits
for the data collection task to send a batch file back to the PC. Once it 
recieves this batch file, the file deconstructs it into step response data in
the form of an array. A plot is created using matplotlib and a .csv file is
created of the data.

@date Created on Thu Feb  4 02:41:31 2021

@author: justin slavick
'''

## Import needed classes
import serial
import csv
import array
import time
import matplotlib.pyplot as plt
import numpy as np

## Constructs a serial port that allows values to be written to and read from
# the Nucleo
ser = serial.Serial(port='COM3',baudrate=115273,timeout=1)
## Constructs an object that creates the .csv file used to hold the data
writer = csv.writer(open('Lab0x03Data.csv', 'w', newline=''))

## User Input giving instructions to start data collection
start = input('Press "G" to begin recording data: ')
## Once the input is detected, the serial encodes the string into ASCII and
# writes that value to the Nucleo
ser.write(str(start).encode('ascii'))
myval = ''
while(myval == ''):
    myval = ser.readline().decode('ascii')


## Converts the myval string into an array
my_arr = myval.split(',')
## Removes the MicroPython text in the string
my_arr.pop()
## Creates empty time and voltage arrays
time_array = []
volt_array = []
for i in range(200):
    ## Builds the time array based on the size of the step response data
    time_array.append(i*10)
    ## Converts the string ADC values from the Nucleo into in voltage values
    # by multiplying by the max voltage over the max ADC value
    volt_array.append(int(my_arr[i])*(3.3/4095))
    

## Creates and formats the plot
fig = plt.figure()
plt.xlabel('Time (us)')
plt.ylabel('Analog Voltage (V)')
plt.title('Step Response of Analog-Digital Converter')
plt.plot(time_array, volt_array)

## Formats the .csv file
time_stamp = time.ctime()
my_arr.insert(0, time_stamp)
writer.writerow(my_arr)